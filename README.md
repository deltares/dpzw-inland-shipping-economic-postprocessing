# DPZW - Inland shipping economic postprocessing

This repository contains scripts to compute the economic effects of draught on the inland shipping sector. 
It is developed by Ecorys and Deltares for usage in the Deltaprogramma Zoet Water (DPZW) and used to post-process model results from BIVAS.

More details of the module are documented in:

- Conceptual description: Schasfoort, F., J.S. de Jong en E. Meijers (2019) Effectmodules in het Deltaprogramma Zoetwater. Van hydrologisch effect naar economisch effect van droogte. Deltares rapport 11203734-000-ZWS-0010. Download available at: https://www.deltaprogramma.nl/documenten/publicaties/2019/09/20/effectmodules-in-het-deltaprogramma-zoetwater-2019
- Technical documentation: Schasfoort, F., L. Leunge, J.S. de Jong (2020). Documentatie economische effectmodule scheepvaart. Deltares memo 11205272-005-ZWS-0001 d.d. 8 december 2020
